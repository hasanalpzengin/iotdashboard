var mysql = require('../db/mysql');

class ApiController {

    makeLogin(req, res) {
        res.setHeader('Content-Type', 'application/json');
        if (typeof req.body.username == 'undefined' || typeof req.body.password == 'undefined') {
            this.sendError(504, "password or username can't be empty", err)
            return;
        } else {
            mysql().getConnection((err, sqlConn) => {
                if (err) {
                    this.sendError(404, "unable to reach database server", err);
                    return;
                }
                //create sql
                var data = [req.body.username, md5(req.body.password)];
                var sql = "SELECT * FROM users WHERE username=? AND password= ?";
                sqlConn.query(sql, data, function (err, result) {
                    if (err) {
                        this.sendError(504, "error occured while running sql", err);
                        return;
                    }
                    //is login success
                    if (result.length == 0) {
                        res.send(JSON.stringify({
                            error: 1,
                            message: "Error Login"
                        }));
                        return;
                    }
                    console.log(req.body.username + "'s Login Success");
                    //update last login
                    var date = new Date().toISOString().slice(0, 19).replace('T', ' ');
                    //setup session
                    req.session.uid = result[0].id;
                    req.session.utype = result[0].user_type;
                    req.session.group = result[0].group;
                    //create sql data
                    var data = [date, result[0].id];
                    //create sql string
                    var sql = "UPDATE users SET last_login=? WHERE id=?";

                    mysql().getConnection((err, sqlConn) => {
                        sqlConn.query(sql, data, function (err, result) {
                            if (err) throw err;
                            res.send(JSON.stringify(result[0]));
                        });
                    });

                });
            });
        }
    }

    listDevices(req, res) {
        res.setHeader('Content-Type', 'application/json');
        mysql().getConnection((err, sqlConn) => {
            if (err) {
                this.sendError(404, "unable to reach database server", err);
                return;
            }
            var data = [req.session.uid];
            var sql = "SELECT user_devices.dserial as `serial_number`, devices.name, device_types.image, is_online, DATE_FORMAT(last_connection, '%d/%m/%Y') as last_connection FROM user_devices, devices, device_types WHERE devices.type=device_types.id AND dserial=devices.serial_number AND uid=?";
            sqlConn.query(sql, data, function (err, result) {
                if (err) {
                    this.sendError(404, "error occured while running sql", err);
                    return;
                }
                mysql().getConnection((err, sqlConn) => {
                    if (err) {
                        this.sendError(404, "unable to reach database server", err);
                        return;
                    }
                    data = [req.session.group];
                    sql = "SELECT devices.name, serial_number FROM devices, device_groups WHERE dserial=devices.serial_number AND gid=?";
                    sqlConn.query(sql, data, function (err, devices) {
                        if (err) {
                            this.sendError(504, "error occured while running sql", err);
                            return;
                        }
                        //get threshold of all devices
                        sql = "SELECT dserial, min_value, max_value FROM user_devices WHERE uid=?";
                        data = [req.session.uid];
                        sqlConn.query(sql, data, (err, thresholds) => {
                            if (err) {
                                this.sendError(504, "error occured while running sql", err);
                                return;
                            }
                            thresholds.forEach(threshold => {
                                res.cookie(threshold.dserial + "@min_threshold", threshold.min_value);
                                res.cookie(threshold.dserial + "@max_threshold", threshold.max_value);
                                console.log("Cookie Set");
                            });
                            res.send(JSON.stringify(devices));
                        });
                    });
                });
            });
            sqlConn.release();
        });
    }

    viewDevice(req, res) {
        res.setHeader('Content-Type', 'application/json');
        if (typeof req.session.uid != "undefined") {
            this.getDevice(req.params.serial_number, function (err, result) {
                if (err) {
                    this.sendError(404, "unable to reach database server", err);
                    return;
                }
                var device_data = result[0];
                if (typeof device_data == 'undefined' || device_data.length == 0) {
                    this.sendError(503, "undefined device request", err);
                    return;
                }
                mysql().getConnection((err, sqlConn) => {
                    var sql = "SELECT * FROM device_logs WHERE dserial=? ORDER BY created_at DESC";
                    var data = [device_data['serial_number']];
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) {
                            this.sendError(404, "error occured while running sql", err);
                            return;
                        }
                        sql = "SELECT min_value, max_value FROM user_devices WHERE dserial=? AND uid=?";
                        data = [device_data['serial_number'], req.session.uid];
                        sqlConn.query(sql, data, (err, threshold) => {
                            if (err) {
                                this.send(404, "error occured while running sql", err)
                                return;
                            }
                            //set cookie
                            if (threshold.length != 0) {
                                res.cookie(device_data['serial_number'] + '@min_threshold', threshold[0].min_value);
                                res.cookie(device_data['serial_number'] + '@max_threshold', threshold[0].max_value);
                            }
                            //return json
                            res.send(JSON.stringify(device_data));
                        });
                    });
                    sqlConn.release();
                });
            });
        }
    }
    //get specific device model
    getDevice(serial_number, callback) {
        mysql().getConnection((err, sqlConn) => {
            var sql = "SELECT serial_number, DATE_FORMAT(last_connection, '%d/%m/%Y') as last_connection, devices.name as name, device_types.name as type_name, device_types.image, is_online FROM devices, device_types WHERE serial_number=? AND devices.type=device_types.id";
            var data = [serial_number];
            sqlConn.query(sql, data, function (err, result) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, result);
                }
            });
            sqlConn.release();
        });
    }

    loginCheck(req, res) {
        res.setHeader('Content-Type', 'application/json');
        var status = (typeof req.session.uid == "undefined") ? false : true;
        res.send(JSON.stringify({
            status: status
        }));
    }

    filterValues(req, res) {
        res.setHeader('Content-Type', 'application/json');
        mysql().getConnection((err, sqlConn) => {
            if (err) {
                this.sendError(404, "unable to reach database server", err);
                return;
            };
            var sql;
            if (req.session.utype == "admin") {
                if (req.body.interval == "day") {
                    sql = "SELECT message, created_at FROM device_logs, user_devices WHERE user_devices.dserial=? AND device_logs.created_at>=NOW() - INTERVAL 1 DAY"
                } else if (req.body.interval == "week") {
                    sql = "SELECT message, created_at FROM device_logs, user_devices WHERE user_devices.dserial=? AND device_logs.created_at>=NOW() - INTERVAL 1 WEEK"
                } else if (req.body.interval == "month") {
                    sql = "SELECT message, created_at FROM device_logs, user_devices WHERE user_devices.dserial=? AND device_logs.created_at>=NOW() - INTERVAL 1 MONTH"
                } else if (req.body.interval == "all") {
                    sql = "SELECT message, created_at FROM device_logs, user_devices WHERE user_devices.dserial=?"
                } else {
                    this.sendError(503, "undefined interval request", err);
                    return;
                }
            } else {
                if (req.body.interval == "day") {
                    sql = "SELECT message, created_at FROM device_logs, user_devices WHERE user_devices.dserial=? AND device_logs.dserial=user_devices.dserial AND user_devices.uid=? AND device_logs.created_at>=NOW() - INTERVAL 1 DAY"
                } else if (req.body.interval == "week") {
                    sql = "SELECT message, created_at FROM device_logs, user_devices WHERE user_devices.dserial=? AND device_logs.dserial=user_devices.dserial AND user_devices.uid=? AND device_logs.created_at>=NOW() - INTERVAL 1 WEEK"
                } else if (req.body.interval == "month") {
                    sql = "SELECT message, created_at FROM device_logs, user_devices WHERE user_devices.dserial=? AND device_logs.dserial=user_devices.dserial AND user_devices.uid=? AND device_logs.created_at>=NOW() - INTERVAL 1 MONTH"
                } else if (req.body.interval == "all") {
                    sql = "SELECT message, created_at FROM device_logs, user_devices WHERE user_devices.dserial=? AND device_logs.dserial=user_devices.dserial AND user_devices.uid=?"
                } else {
                    this.sendError(503, "undefined interval request", err);
                    return;
                }
            }

            var data = [req.body.serial_number, req.session.uid];
            sqlConn.query(sql, data, (err, result) => {
                if (err) {
                    this.sendError(504, "error occured while running sql", err);
                    return;
                }

                res.send(JSON.stringify({
                    result: result
                }));
            });
        });
    }

    sendError(err_code, err_msg, err) {
        res.send(JSON.stringify({
            err_code: err_code,
            err_msg: err_msg,
            err: err
        }));
    }
}

module.exports = ApiController;