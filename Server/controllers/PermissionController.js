var mysql = require('../db/mysql');

class PermissionController {

    isPermissionExist(req, permission_name, callback) {
        mysql().getConnection((err, sqlConn) => {
            if (err) throw err;
            var sql = "SELECT permissions.id FROM permissions, user_permissions WHERE permissions.id=user_permissions.permission_id AND user_permissions.uid=? AND permissions.name=?";
            var data = [req.session.uid, permission_name];

            sqlConn.query(sql, data, (err, result) => {
                if (err) throw err;
                if (result.length == 0) {
                    callback(false);
                } else {
                    callback(true);
                }
            });
        });
    }
}

module.exports = PermissionController;