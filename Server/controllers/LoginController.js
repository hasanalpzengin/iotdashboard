const md5 = require('md5');
var mysql = require('../db/mysql');

class LoginController {

    makeLogin(req, res, callback) {
        if (typeof req.body.username == 'undefined' || typeof req.body.password == 'undefined') {
            res.render('pages/login', {
                error: true
            });
            return;
        } else {
            mysql().getConnection((err, sqlConn) => {
                if (err) throw err;
                //create sql
                var data = [req.body.username, md5(req.body.password)];
                var sql = "SELECT * FROM users WHERE username=? AND password= ?";
                sqlConn.query(sql, data, function (err, result) {
                    if (err) throw err;
                    //is login success
                    if (result.length == 0) {
                        res.render('pages/login', {
                            error: true
                        });
                        return;
                    }
                    console.log(req.body.username + "'s Login Success");
                    //update last login
                    var date = new Date().toISOString().slice(0, 19).replace('T', ' ');
                    //setup session
                    req.session.uid = result[0].id;
                    req.session.utype = result[0].user_type;
                    req.session.group = result[0].group;
                    //create sql data
                    var data = [date, result[0].id];
                    //create sql string
                    var sql = "UPDATE users SET last_login=? WHERE id=?";

                    mysql().getConnection((err, sqlConn) => {
                        sqlConn.query(sql, data, function (err, result) {
                            if (err) callback(err, false);
                            //last_login updated
                        });
                    });

                    callback(false, req.session);

                });
            });
        }
    }

    getIndex(req, res) {
        mysql().getConnection((err, sqlConn) => {
            var data = [req.session.uid];
            var sql = "SELECT * FROM users WHERE id=?";
            sqlConn.query(sql, data, function (err, result) {
                if (err) throw err;
                if (result.length > 0) {
                    //get first data
                    result = result[0];
                    res.render('pages/index', {
                        data: result,
                        coordinates: []
                    });
                } else {
                    res.render('pages/login', {
                        error: true
                    });
                }
            });
        });
    }

    getSettings(req, res) {
        mysql().getConnection((err, sqlConn)=>{
            var data = [req.session.uid];
            var sql = "SELECT name, surname, mail, birth_date FROM users WHERE id=?";
            sqlConn.query(sql, data, (err, result)=>{
                res.render('pages/settings', {
                    user: result[0]
                });
            });
        });
    }

    editSettings(req, res){
        mysql().getConnection((err, sqlConn)=>{
            var data = [req.body.name, req.body.surname, req.body.mail, req.body.birth_date, req.session.uid];
            var sql = "UPDATE users SET name=?, surname=?, mail=?, birth_date=? WHERE id=?";
            sqlConn.query(sql, data, (err, result)=>{
                if(err) throw err;
                res.redirect(req.get('referer'));
            });
        });
    }

    disconnect(req, res) {
        delete req.session.uid;
        delete req.session.utype;
        delete req.session.group;

        res.redirect("/login");
    }

}

module.exports = LoginController;