var mysql = require('../../db/mysql');

class GroupController {

    constructor() {
        var file = require("./PermissionController");
        this.permissionController = new file();
    }

    listGroups(req, res) {
        this.permissionController.isPermissionExist(req, "view group", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                mysql().getConnection((err, sqlConn) => {
                    if (err) throw err;
                    var sql = "SELECT * from groups";
                    sqlConn.query(sql, function (err, result) {
                        if (err) throw err;
                        res.render('pages/system-control/groups', {
                            results: result
                        });
                        return;
                    });
                });
            }
        });
    }

    removeGroup(req, res) {
        this.permissionController.isPermissionExist(req, "edit group", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                mysql().getConnection((err, sqlConn) => {
                    var data = [req.body.removeGroup];
                    var sql = "DELETE FROM groups WHERE id=?";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        res.redirect(req.get('referer'));
                    });
                    sqlConn.release();
                });
            }
        });
    }

    addGroup(req, res) {
        this.permissionController.isPermissionExist(req, "add group", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                mysql().getConnection((err, sqlConn) => {
                    if (err) throw err;
                    var data = [req.body.addGroup];
                    var sql = "INSERT INTO groups(name) VALUES(?)";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        res.redirect(req.get('referer'));
                    });
                    sqlConn.release();
                });
            }
        });
    }
}

module.exports = GroupController;