var mysql = require('../../db/mysql');

class ControllerController {

    constructor(){
        var file = require("./PermissionController");
        this.permissionController = new file();
    }

    listControllers(req, res) {
        this.permissionController.isPermissionExist(req, "view controller", (isExist)=>{
            if(!isExist){
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                mysql().getConnection((err, sqlConn) => {
                    if (err) throw err;
                    var sql = "SELECT * from controllers";
                    sqlConn.query(sql, function (err, result) {
                        if (err) throw err;
                        res.render('pages/system-control/controllers', {
                            results: result
                        });
                        return;
                    });
                    sqlConn.release();
                });
            }
        });
    }

    addController(req, res) {
        this.permissionController.isPermissionExist(req, "add controller", (isExist)=>{
            if(!isExist){
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                mysql().getConnection((err, sqlConn) => {
                    var data = [req.body.addController];
                    var sql = "INSERT INTO controllers(name) VALUES(?)";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        res.redirect(req.get('referer'));
                    });
                    sqlConn.release();
                });
            }
        });
    }

    removeController(req, res) {
        this.permissionController.isPermissionExist(req, "edit controller", (isExist)=>{
            if(!isExist){
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                mysql().getConnection((err, sqlConn) => {
                    var data = [req.body.removeController];
                    var sql = "DELETE FROM controllers WHERE id=?";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        res.redirect(req.get('referer'));
                    });
                    sqlConn.release();
                });
            }
        });
    }
}

module.exports = ControllerController;