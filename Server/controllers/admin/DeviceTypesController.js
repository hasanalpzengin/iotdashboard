var mysql = require('../../db/mysql');

class DeviceTypesController {

    constructor() {
        var file = require("./PermissionController");
        this.permissionController = new file();
    }

    listDeviceTypes(req, res) {
        this.permissionController.isPermissionExist(req, "view device type", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                mysql().getConnection((err, sqlConn) => {
                    var sql = "SELECT device_types.id, device_types.name, image, controllers.name as controller_name, min_value, max_value FROM device_types, controllers WHERE controller=controllers.id";
                    sqlConn.query(sql, function (err, device_type_results) {
                        if (err) throw err;
                        sqlConn.release();
                        mysql().getConnection((err, sqlConn) => {
                            sql = "SELECT id, name FROM controllers";
                            sqlConn.query(sql, function (err, controller_results) {
                                if (err) throw err;
                                res.render('pages/system-control/device_types', {
                                    results: device_type_results,
                                    controllers: controller_results
                                });
                            });
                            sqlConn.release();
                        });
                    });
                });
            }
        });
    }

    addDeviceType(req, res) {
        this.permissionController.isPermissionExist(req, "add device type", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                //image proccess
                mysql().getConnection((err, sqlConn) => {
                    var data, sql;
                    if (typeof req.files.image != 'undefined') {
                        var image = req.files.image;
                        var image_name = image.name;
                        image.mv("public/img/device-types/" + image_name, function (err) {
                            if (err) throw err;
                        });
                        data = [req.body.name, image_name, req.body.controller, req.body.min_value, req.body.max_value];
                        sql = "INSERT INTO device_types(name, image, controller, min_value, max_value) VALUES(?,?,?,?,?)";
                    } else {
                        data = [req.body.name, req.body.controller, req.body.min_value, req.body.max_value];
                        sql = "INSERT INTO device_types(name, controller, min_value, max_value) VALUES(?,?,?,?)";
                    }
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        res.redirect(req.get('referer'));
                    });
                });
            }
        });
    }

    removeDeviceType(req, res) {
        this.permissionController.isPermissionExist(req, "edit device type", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                mysql().getConnection((err, sqlConn) => {
                    if (err) throw err;
                    var data = [req.body.removeDeviceType];
                    var sql = "DELETE FROM device_types WHERE id=?";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        res.redirect(req.get('referer'));
                    });
                });
            }
        });
    }
}

module.exports = DeviceTypesController;