var mysql = require('../../db/mysql');

class DeviceController {

    constructor() {
        var file = require("./PermissionController");
        this.permissionController = new file();
    }

    listDevices(req, res) {
        this.permissionController.isPermissionExist(req, "view device", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                mysql().getConnection((err, sqlConn) => {
                    if (err) throw err;
                    var sql = "SELECT serial_number, devices.name, last_connection, device_types.name as type_name FROM devices, device_types WHERE devices.type = device_types.id";
                    sqlConn.query(sql, function (err, result) {
                        if (err) throw err;
                        sqlConn.release();
                        mysql().getConnection((err, sqlConn) => {
                            if(err) throw err;
                            sql = "SELECT * FROM device_types";
                            sqlConn.query(sql, function (err, device_types) {
                                if (err) throw err;
                                res.render('pages/system-control/devices', {
                                    results: result,
                                    device_types: device_types
                                });
                                sqlConn.release();
                            });
                        });
                    });
                });
            }
        });
    }

    viewDevice(req, res) {
        this.permissionController.isPermissionExist(req, "view device", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                mysql().getConnection((err, sqlConn) => {
                    var data = [req.params.serial_number];
                    var sql = "SELECT * FROM devices WHERE serial_number=?";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        sqlConn.release();
                        mysql().getConnection((err, sqlConn) => {
                            sql = "SELECT id, name from device_types";
                            sqlConn.query(sql, function (err, types) {
                                res.render('pages/system-control/edit_device', {
                                    result: result[0],
                                    types: types
                                });
                            });
                            sqlConn.release();
                        });
                    });
                });
            }
        });
    }

    removeDevice(req, res) {
        this.permissionController.isPermissionExist(req, "edit device", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                mysql().getConnection((err, sqlConn) => {
                    var data = [req.body.removeDevice];
                    var sql = "DELETE FROM devices WHERE serial_number=?";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        res.redirect(req.get('referer'));
                    });
                    sqlConn.release();
                });
            }
        });
    }

    addDevice(req, res) {
        this.permissionController.isPermissionExist(req, "add device", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                mysql().getConnection((err, sqlConn) => {
                    var data = [req.body.serial_number, req.body.name, req.body.type];
                    var sql = "INSERT INTO devices(serial_number, name, type) VALUES(?, ?, ?)";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        res.redirect(req.get('referer'));
                    });
                    sqlConn.release();
                });
            }
        });
    }

    editDevice(req, res) {
        this.permissionController.isPermissionExist(req, "edit device", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                if (typeof req.body.serial_number != 'undefined') {
                    mysql().getConnection((err, sqlConn) => {
                        if (err) throw err;
                        var data = [req.body.name, req.body.type, req.body.serial_number];
                        var sql = "UPDATE devices SET name=?, type=? WHERE serial_number=?";
                        sqlConn.query(sql, data, function (err, result) {
                            if (err) throw err;
                            res.redirect(req.get('referer'));
                        });
                        sqlConn.release();
                    });
                }
            }
        });
    }
}

module.exports = DeviceController;