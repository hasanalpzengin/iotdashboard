const md5 = require('md5');
var mysql = require('../../db/mysql');

class UserController {

    constructor() {
        var file = require("./PermissionController");
        this.permissionController = new file();
    }

    listUsers(req, res) {
        this.permissionController.isPermissionExist(req, "view user", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                mysql().getConnection((err, sqlConn) => {
                    var sql = "SELECT users.id, username, password, users.name as name, surname, mail, DATE_FORMAT(birth_date, '%d/%m/%Y') as birth_date, permission, DATE_FORMAT(last_login, '%d/%m/%Y %H:%i') as last_login, is_active, groups.name as group_name from users, groups where users.group = groups.id";
                    sqlConn.query(sql, function (err, user_results) {
                        if (err) throw err;
                        mysql().getConnection((err, sqlConn) => {
                            sql = "SELECT id, name FROM groups";
                            sqlConn.query(sql, function (err, group_results) {
                                if (err) throw err;
                                res.render('pages/system-control/users', {
                                    results: user_results,
                                    groups: group_results
                                });
                            });
                            sqlConn.release();
                        });
                    });
                    sqlConn.release();
                });
            }
        });
    }

    addUser(req, res) {
        this.permissionController.isPermissionExist(req, "add user", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                mysql().getConnection((err, sqlConn) => {
                    var data = [req.body.uname, md5(req.body.upass), req.body.name, req.body.surname, req.body.mail, req.body.birth_date, req.body.group, req.body.user_type]
                    var sql = "INSERT INTO users(username, password, name, surname, mail, birth_date, `group`, user_type) VALUES(?, ?, ?, ?, ?, ?, ?, ?)"
                    console.log(sql);
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        this.permissionController.createFirstPermissions(req, res, result.insertId);
                        //redirect
                        res.redirect(req.get('referer'));
                    });
                    sqlConn.release();
                });
            }
        });
    }

    getEditUser(req, res) {
        this.permissionController.isPermissionExist(req, "edit user", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                mysql().getConnection((err, sqlConn) => {
                    if (err) throw err;
                    var data = [req.params.id];
                    var sql = "SELECT users.id, username, users.name as name, surname, mail, DATE_FORMAT(birth_date, '%d/%m/%Y') as birth_date, permission, `group`, user_type from users, groups where users.group = groups.id AND users.id = ?";
                    sqlConn.query(sql, data, function (err, user_results) {
                        if (err) throw err;
                        mysql().getConnection((err, sqlConn) => {
                            sql = "SELECT id, name FROM groups";
                            sqlConn.query(sql, function (err, group_results) {
                                if (err) throw err;
                                res.render('pages/system-control/edit_user', {
                                    result: user_results[0],
                                    groups: group_results
                                });
                            });
                            sqlConn.release();
                        });
                    });
                    sqlConn.release();
                });
            }
        });
    }

    editUser(req, res) {
        this.permissionController.isPermissionExist(req, "edit user", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                mysql().getConnection((err, sqlConn) => {
                    if (err) throw err;
                    if(req.body.password.length==0){
                        var data = [req.body.username, req.body.name, req.body.surname, req.body.mail, req.body.birth_date, req.body.permission, req.body.group, req.body.user_type, req.body.id];
                        var sql = "UPDATE users SET username=?, name=?, surname=?, mail=?, birth_date=?, permission=?, `group`=?, user_type=? WHERE id=?";
                    }else{
                        var data = [req.body.username, md5(req.body.password), req.body.name, req.body.surname, req.body.mail, req.body.birth_date, req.body.permission, req.body.group, req.body.user_type, req.body.id];
                        var sql = "UPDATE users SET username=?, password=?, name=?, surname=?, mail=?, birth_date=?, permission=?, `group`=?, user_type=? WHERE id=?";
                    }
                    sqlConn.query(sql, data, function (err, results) {
                        if (err) throw err;
                        res.redirect(req.get('referer'));
                    });
                    sqlConn.release();
                });
            }
        });
    }

    removeUser(req, res) {
        this.permissionController.isPermissionExist(req, "edit user", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                mysql().getConnection((err, sqlConn) => {
                    var data = [req.body.removeUser];
                    var sql = "DELETE FROM users WHERE id=?";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        this.permissionController.clearPermissions(req, res);
                        res.redirect(req.get('referer'));
                    });
                    sqlConn.release();
                });
            }
        });
    }
}

module.exports = UserController;