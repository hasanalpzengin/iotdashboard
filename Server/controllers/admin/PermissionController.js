var mysql = require('../../db/mysql');

class PermissionController {

    showEditPermissions(req, res) {
        this.isPermissionExist(req, "view user permission", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                mysql().getConnection((err, sqlConn) => {
                    if (err) throw err;
                    var existList = [];
                    var data = [req.params.id];
                    var sql = "SELECT permissions.id, permissions.name FROM permissions, user_permissions WHERE user_permissions.uid=? AND user_permissions.permission_id=permissions.id";
                    sqlConn.query(sql, data, function (err, user_result) {
                        if (err) throw err;
                        mysql().getConnection((err, sqlConn) => {
                            var sql = "SELECT * FROM permissions";
                            sqlConn.query(sql, (err, result) => {
                                result.forEach((element) => {
                                    var isExist = false;
                                    user_result.forEach((uresult) => {
                                        if (uresult.id == element.id) {
                                            isExist = true;
                                            return;
                                        }
                                    });
                                    existList[element.id] = isExist;
                                });
                                res.render('pages/system-control/userpermissions', {
                                    results: result,
                                    existList: existList,
                                    uid: req.params.id
                                });
                            });
                            sqlConn.release();
                        })
                    });
                    sqlConn.release();
                });
            }
        });
    }

    editPermissions(req, res) {
        this.isPermissionExist(req, "edit user permission", (isExist) => {
            if (!isExist) {
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            } else {
                mysql().getConnection((err, sqlConn) => {
                    var sql = "DELETE FROM user_permissions WHERE uid=?";
                    var data = [req.body.userID];
                    sqlConn.query(sql, data, (err, result) => {
                        if (err) throw err;
                        //permissions cleared
                    });
                });
                var permissions = req.body.permissions;
                console.log(permissions);

                mysql().getConnection((err, sqlConn) => {
                    if (err) throw err;
                    for (var key in permissions) {
                        if (permissions[parseInt(key)] == "on") {
                            var sql = "INSERT INTO user_permissions(uid, permission_id) VALUES(?,?)";
                            var data = [req.body.userID, parseInt(key) + 1];
                            console.log("UID: " + req.body.userID + " Key: " + key);

                            sqlConn.query(sql, data, function (err, result) {
                                if (err) throw err;
                            });
                        }
                    }
                    sqlConn.release();
                });
                res.redirect(req.get('referer'));
            }
        });
    }

    isPermissionExist(req, permission_name, callback) {
        mysql().getConnection((err, sqlConn) => {
            if (err) throw err;
            var sql = "SELECT permissions.id FROM permissions, user_permissions WHERE permissions.id=user_permissions.permission_id AND user_permissions.uid=? AND permissions.name=?";
            var data = [req.session.uid, permission_name];

            sqlConn.query(sql, data, (err, result) => {
                if (err) throw err;
                if (result.length == 0) {
                    callback(false);
                } else {
                    callback(true);
                }
            });
        });
    }

    clearPermissions(req, res) {
        mysql().getConnection((err, sqlConn) => {
            var sql = "DELETE FROM user_permissions WHERE uid=?";
            var data = [req.body.removeUser];
            sqlConn.query(sql, data, (err, result) => {
                if (err) throw err;
                //permissions cleared
            });
        });
    }

    createFirstPermissions(req, res, insert_id) {
        if (req.body.user_type === "admin") {
            var defaultAdminPermissions = [
                "add controller",
                "view controller",
                "add device group",
                "view device group",
                "view device log",
                "view device type",
                "add device type",
                "add device",
                "view device",
                "add group",
                "view group",
                "add user-device",
                "view user-device",
                "add user",
                "view user",
            ];
            //insert default permissions
            mysql().getConnection((err, sqlConn) => {
                if (err) throw err;
                defaultAdminPermissions.forEach(element => {
                    var sql = "INSERT INTO user_permissions(uid, permission_id) VALUES(?, ?)";
                    var data = [insert_id, element];
                    sqlConn.query(sql, data, (err, result) => {
                        if (err) throw err;
                    })
                });
                sqlConn.release();
            });
        }
    }
}

module.exports = PermissionController;