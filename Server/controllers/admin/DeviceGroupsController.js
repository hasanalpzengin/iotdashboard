var mysql = require('../../db/mysql');

class DeviceGroupsController {

    constructor(){
        var file = require("./PermissionController");
        this.permissionController = new file();
    }

    viewDeviceGroups(req, res) {
        this.permissionController.isPermissionExist(req, "view device group", (isExist)=>{
            if(!isExist){
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                mysql().getConnection((err, sqlConn) => {
                    if (err) throw err;
                    var data = [req.params.serial_number];
                    var sql = "SELECT gid, groups.name as group_name FROM device_groups, groups WHERE groups.id = gid AND dserial=?";
                    sqlConn.query(sql, data, function (err, results) {
                        if (err) throw err;
                        mysql().getConnection((err, sqlConn) => {
                            sql = "SELECT * from groups";
                            sqlConn.query(sql, function (err, groups) {
                                if (err) throw err;
                                res.render('pages/system-control/device_groups', {
                                    results: results,
                                    groups: groups,
                                    device_serial: req.params.serial_number
                                });
                            });
                            sqlConn.release();
                        });
                    });
                });
            }
        });
    }

    addDeviceGroup(req, res) {
        this.permissionController.isPermissionExist(req, "add device group", (isExist)=>{
            if(!isExist){
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                mysql().getConnection((err, sqlConn) => {
                    var data = [req.body.addGroup, req.body.device_serial];
                    var sql = "INSERT INTO device_groups(gid, dserial) VALUES(?,?)";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        res.redirect(req.get('referer'));
                    });
                    sqlConn.release();
                });
            }
        });
    }

    removeDeviceGroup(req, res) {
        this.permissionController.isPermissionExist(req, "edit device group", (isExist)=>{
            if(!isExist){
                res.render('pages/error', {
                    error: "Access not permitted."
                });
                return;
            }else{
                mysql().getConnection((err, sqlConn) => {
                    var data = [req.body.removeGroup, req.body.device_serial];
                    var sql = "DELETE FROM device_groups WHERE gid=? AND dserial=?";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        res.redirect(req.get('referer'));
                    });
                });
            }
        });
    }
}

module.exports = DeviceGroupsController;