class LogController {
    showHistory(req, res) {
        if (typeof req.session.uid != "undefined") {
            //init if logged
            var mysql = require('../db/mysql');
            if (typeof req.session.utype=="admin") {
                mysql().getConnection((err, sqlConn) => {
                    var sql = "SELECT uid, username, type, log FROM users, user_logs WHERE users.id=user_logs.uid";
                    sqlConn.query(sql, (err, result) => {
                        if (err) throw err;
                        res.render('pages/history', {
                            results: result
                        });
                    });
                });
            } else {
                mysql().getConnection((err, sqlConn) => {
                    var data = [req.session.uid];
                    var sql = "SELECT uid, username, type, log FROM users, user_logs WHERE users.id=user_logs.uid AND users.id=?";
                    sqlConn.query(sql, data, (err, result) => {
                        if (err) throw err;
                        res.render('pages/history', {
                            results: result
                        });
                    });
                });
            }
        }
    }
}

module.exports = LogController;