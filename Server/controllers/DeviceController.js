var mysql = require('../db/mysql');

class DeviceController {
    listDevices(req, res) {
        if (req.session.utype == "admin") {
            mysql().getConnection((err, sqlConn) => {
                if (err) throw err;
                var sql = "SELECT serial_number, DATE_FORMAT(last_connection, '%d/%m/%Y') as last_connection, devices.name as `name`, is_online, image FROM devices, device_types WHERE devices.type=device_types.id";
                sqlConn.query(sql, function (err, result) {
                    if (err) throw err;
                    res.render('pages/devices', {
                        results: result
                    });
                });
                sqlConn.release();
            });
        } else {
            mysql().getConnection((err, sqlConn) => {
                if (err) throw err;
                var data = [req.session.uid];
                var sql = "SELECT user_devices.dserial as `serial_number`, devices.name, device_types.image, is_online, DATE_FORMAT(last_connection, '%d/%m/%Y') as last_connection FROM user_devices, devices, device_types WHERE devices.type=device_types.id AND dserial=devices.serial_number AND uid=?";
                sqlConn.query(sql, data, function (err, result) {
                    if (err) throw err;
                    mysql().getConnection((err, sqlConn) => {
                        data = [req.session.group];
                        sql = "SELECT devices.name, serial_number FROM devices, device_groups WHERE dserial=devices.serial_number AND gid=?";
                        sqlConn.query(sql, data, function (err, devices) {
                            if (err) throw err;
                            //get threshold of all devices
                            sql = "SELECT dserial, min_value, max_value FROM user_devices WHERE uid=?";
                            data = [req.session.uid];
                            sqlConn.query(sql, data, (err, thresholds) => {
                                if (err) throw err;
                                thresholds.forEach(threshold => {
                                    res.cookie(threshold.dserial + "@min_threshold", threshold.min_value);
                                    res.cookie(threshold.dserial + "@max_threshold", threshold.max_value);
                                    console.log("Cookie Set");
                                });
                                res.render('pages/devices', {
                                    results: result,
                                    devices: devices
                                });
                            });
                        });
                    });
                });
                sqlConn.release();
            });
        }
    }

    viewDevice(req, res) {
        if (typeof req.session.uid != "undefined") {
            this.getDevice(req.params.serial_number, function (err, result) {
                var device_data = result[0];
                if (typeof device_data == 'undefined' || device_data.length == 0) {
                    res.render('pages/error', {
                        error: 'This Device is not Exist'
                    });
                    return;
                }
                mysql().getConnection((err, sqlConn) => {
                    var sql = "SELECT * FROM device_logs WHERE dserial=? ORDER BY created_at DESC";
                    var data = [device_data['serial_number']];
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        sql = "SELECT min_value, max_value FROM user_devices WHERE dserial=? AND uid=?";
                        data = [device_data['serial_number'], req.session.uid];
                        sqlConn.query(sql, data, (err, threshold) => {
                            if (err) throw err;
                            //set cookie
                            if(threshold.length!=0){
                                res.cookie(device_data['serial_number'] + '@min_threshold', threshold[0].min_value);
                                res.cookie(device_data['serial_number'] + '@max_threshold', threshold[0].max_value);
                            }
                            //set cookie
                            res.render('pages/device', {
                                device: device_data,
                                logs: result
                            });
                        });
                    });
                    sqlConn.release();
                });
            });
        }
    }
    //get specific device model
    getDevice(serial_number, callback) {
        mysql().getConnection((err, sqlConn) => {
            var sql = "SELECT serial_number, DATE_FORMAT(last_connection, '%d/%m/%Y') as last_connection, devices.name as name, device_types.name as type_name, device_types.image, is_online FROM devices, device_types WHERE serial_number=? AND devices.type=device_types.id";
            var data = [serial_number];
            sqlConn.query(sql, data, function (err, result) {
                if (err) {
                    callback(err, null);
                } else {
                    callback(null, result);
                }
            });
            sqlConn.release();
        });
    }

    removeDevice(req, res) {
        mysql().getConnection((err, sqlConn) => {
            var data = [req.body.removeSerial, req.session.uid];
            var sql = "DELETE FROM user_devices WHERE dserial=? AND uid=?";
            sqlConn.query(sql, data, function (err, result) {
                if (err) throw err;
                res.redirect(req.get('referer'));
            });
            sqlConn.release();
        });
    }

    addDevice(req, res) {
        mysql().getConnection((err, sqlConn) => {
            if (err) throw err;
            var data = [req.body.addSerial, req.session.uid];
            var sql = "INSERT INTO user_devices(dserial, uid) VALUES(?, ?)";
            console.log(sql);
            sqlConn.query(sql, data, function (err, result) {
                if (err) throw err;
                res.redirect(req.get('referer'));
            });
            sqlConn.release();
        });
    }

    editSettings(req, res) {
        mysql().getConnection((err, sqlConn) => {
            var sql = "DELETE FROM user_devices WHERE uid=? AND dserial=?";
            var data = [req.session.uid, req.body.device];
            sqlConn.query(sql, data, (err, result) => {
                if (err) throw err;
            });
            /* TODO */
            //add condition for can user edit that device
            sql = "INSERT INTO user_devices(uid, dserial, min_value, max_value) VALUES(?,?,?,?)"
            data = [req.session.uid, req.body.device, req.body.min, req.body.max];
            sqlConn.query(sql, data, (err, result) => {
                if (err) throw err;
                res.redirect(req.get('referer'));
            });
        });
    }

    deviceSettings(req, res) {
        mysql().getConnection((err, sqlConn) => {
            var data = [req.params.serial_number, req.session.uid];
            var sql = "SELECT user_devices.min_value as `threshold_min`, user_devices.max_value as `threshold_max`, device_types.min_value, device_types.max_value FROM user_devices, devices, device_types WHERE devices.serial_number=user_devices.dserial AND device_types.id=devices.type AND user_devices.dserial=? AND user_devices.uid=?"
            sqlConn.query(sql, data, (err, result) => {
                if (err) throw err;
                if (result.length != 0) {
                    res.render('pages/device_settings', {
                        result: result[0],
                        constants: {
                            device: req.params.serial_number,
                            user: req.session.uid
                        }
                    });
                } else {
                    res.render('pages/device_settings', {
                        result: {
                            threshold_min: 0,
                            threshold_max: 0,
                            max_value: 99,
                            min_value: -99
                        },
                        constants: {
                            device: req.params.serial_number,
                            user: req.session.uid
                        }
                    });
                }
            });
        });
    }
}

module.exports = DeviceController;