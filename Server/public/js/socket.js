import Client from '/js/Client/Client.js';
var client = new Client();

if ($(location).attr('pathname').includes('devices')) {
    var length = $(location).attr('pathname').length;
    var requestedDevice = $(location).attr('pathname').slice(9, length);
    if (requestedDevice !== "") {
        client.subscribe(requestedDevice);
    } else {
        var deviceCards = document.body.querySelectorAll(".device");
        deviceCards.forEach(function (deviceCard) {
            console.log("Loop");
            if ($(deviceCard).find('canvas').length != 0) {
                client.subscribe($(deviceCard).find('serial').text().trim());
            }
        });
    }
}