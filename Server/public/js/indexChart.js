const maxClient = 500;
var clientCount;
var data1;

$(document).ready(function(){
  var clientCountCanvas = $("#clientCountPie");
  clientCount = parseInt($('#clientAmount').text());

  data1 = {
    labels: ['clients','empty'],
    datasets:[
      {
        label: 'current',
        //500 is server capacity.
        data: [clientCount, maxClient-clientCount],
        backgroundColor:['#4286f4','#f44141']
      }
    ]
  }

  //options
  var options = {
      responsive: true,
      title: {
          display: false
      },
      legend: {
          display: false
      }
    };

  var clientCountPie = new Chart(clientCountCanvas, {
    type: 'pie',
    data: data1,
    options: options
  });

});

function updateClientCount(){
  clientCount = parseInt($('#clientAmount').text());
  data1.datasets[0].data[0]=clientCount;
  data1.datasets[0].data[1]=maxClient-clientCount;
}
