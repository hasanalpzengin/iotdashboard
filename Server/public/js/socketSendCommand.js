import Client from '/js/Client/Client.js';
var client = new Client();

$('#commandForm').submit(()=>{
    var device = $('#device').val();
    var topic = $('#topic').val();
    var command = $('#command').val();
    client.socket.emit('command', device+"@"+topic+"@"+command);
    alert("Command Sent");
});