$(document).ready(function() {
    $('#help').hide();
});

function loginSubmit(){
  var uname = $('#username').val();
  var passw = $('#password').val();
  if(uname.length<=0 || passw.length<=0){
    $('#help').val('Username or password can\'t be empty !');
    $('#help').show();
    return false;
  }
  
  $.post('/',{
    username: uname,
    password: passw
  }).done(function(){
    window.location="/";
  });
}
