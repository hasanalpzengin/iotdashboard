$(document).ready(() => {
    Push.Permission.request();
});

function isNotification(val, serial_number) {
    console.log("Entered");
    console.log("Val: " + val);
    if (getCookie("notification") == "true" || (typeof getCookie("notification")) == "undefined") {       
        if (parseInt(val) > parseInt(getCookie(serial_number + "@max_threshold"))) {
            Push.create(serial_number + '\'s Threshold Max Triggered');
        } else if (parseInt(val) < parseInt(getCookie(serial_number + "@min_threshold"))) {
            Push.create(serial_number + '\'s Threshold Min Triggered');
        }
    }
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}