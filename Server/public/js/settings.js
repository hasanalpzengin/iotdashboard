$("#settings").submit(function (event) {
  var name = $("#name").val();
  var surname = $("#surname").val();
  var mail = $("#mail").val();
  var birth_date = $("#birth-date").val();

  $.post('/settings', {
    name: name,
    surname: surname,
    mail: mail,
    birth_date: birth_date
  }).done(() => {
    window.location.reload();
  });
});

$(document).ready(()=>{
  var status = getCookie("notification");
  if(status=="false"){
    $('#notification-status').text("Deactive");
  }else{
    $('#notification-status').text("Active");
  }
});

$('#notification-button').click(()=>{
  var status = getCookie("notification");
  console.log(document.cookie);
  console.log("Status: "+status);
  
  
  if(status=="false"){
    document.cookie = "notification=true";
    $('#notification-status').text("Active");
  }else{
    document.cookie = "notification=false";
    $('#notification-status').text("Deactive");
  }
});

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}