export default class Client {

  constructor() {
    Client.socket = io();
    //get socketList to list connected devices
    Client.socket.on('connect', this.connect);
    //update socketList
    Client.socket.on('updateClientCount', this.updateSockets);
    Client.socket.on('value', this.addValue);
    Client.socket.on('getId', this.getId);

    //get client id
    Client.socket.emit('getId', true);
  }

  setName(name){
    Client.socket.emit('setName', name);
  }

  getId(id){
    Client.id = id;
    console.log(window.location.pathname);
    console.log(id);
    if(window.location.pathname==="/settings"){
      $('#id').val(Client.id);
    }
  }

  getSocket(){
    Client.socket.emit('getSocket', true);
  }

  connect(){
    console.log("Connected");
    $('#status').removeClass("btn-warning");
    $('#status').addClass("btn-success");
    $('#status-msg').text("Connected");
  }

  updateSockets(amount){
    console.log("SocketList Count Updated");
    $('#clientAmount').text(amount);
    if(window.location.pathname==="/"){updateClientCount()};
  }

  subscribe(serial_number){
    Client.socket.emit("join",serial_number);
  }

  addValue(value){
    console.log("Recieved: "+value);
    var length = $(location).attr('pathname').length;
    var requestedDevice = $(location).attr('pathname').slice(9, length);
    if(requestedDevice.length==0){
      var device = value.split('@')[0]; 
      value = value.split('@')[1];
      if(charts[device].length != 0){
        charts[device].pushValue(value);
        isNotification(value, device);
      }
    }else{
      //specific device message
      var data = value.split('@');
      chart.pushValue(data[1]);
      isNotification(data[1],data[0]);
    }
  }
}