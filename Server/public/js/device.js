$(document).ready(function () {
    chart = createChart($('#chart'));
});

$("#day").click(() => {
    var serial_number = $("serial").text();
    $.post('/api/filterValues', {
        serial_number: serial_number,
        interval: "day"
    }, (data, status) => {
        console.log(data);

        chart.clearChart();
        data.result.forEach((value) => {
            chart.unshiftValue(value.message);
        });
    });
});

$("#week").click(() => {
    var serial_number = $("serial").text();
    $.post('/api/filterValues', {
        serial_number: serial_number,
        interval: "week"
    }, (data, status) => {
        console.log(data);

        chart.clearChart();
        data.result.forEach((value) => {
            chart.unshiftValue(value.message);
        });
    });
});

$("#month").click(() => {
    var serial_number = $("serial").text();
    $.post('/api/filterValues', {
        serial_number: serial_number,
        interval: "month"
    }, (data, status) => {
        console.log(data);

        chart.clearChart();
        data.result.forEach((value) => {
            chart.unshiftValue(value.message);
        });
    });
});

$("#all").click(() => {
    var serial_number = $("serial").text();
    $.post('/api/filterValues', {
        serial_number: serial_number,
        interval: "all"
    }, (data, status) => {
        console.log(data);

        chart.clearChart();
        data.result.forEach((value) => {
            chart.unshiftValue(value.message);
        });
    });
});