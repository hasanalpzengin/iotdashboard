var deviceCards;
var charts = [];

function removeDevice(serial) {
    $.post('/devices', {
        removeSerial: serial
    }).done(function () {
        window.location.reload();
    });
}

function addDevice(serial) {
    $.post('/devices', {
        addSerial: serial
    }).done(function () {
        window.location.reload();
    });
}

$('#addForm').submit(function (event) {
    serialNumber = $('#serial-number').val();
    addDevice(serialNumber);
    event.preventDefault();
});

$('#search').on('input', function () {
    searchString = $('#search').val();
    deviceCards.forEach(function (deviceCard) {
        console.log($(deviceCard).find('serial').val());
        if (!$(deviceCard).find('serial').text().includes(searchString.toUpperCase())) {
            deviceCard.style.display = "none";
        } else {
            deviceCard.style.display = "flex";
        }
    });
});

$(document).ready(function (event) {
    deviceCards = document.body.querySelectorAll(".device");
    deviceCards.forEach(function (deviceCard) {
        if ($(deviceCard).find('canvas').length != 0) {
            charts[$(deviceCard).find('serial').text().trim()] = createChart($(deviceCard).find('canvas'));
            console.log("Entered");
        }
    });
    console.log(charts);
});