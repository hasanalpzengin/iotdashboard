function createChart(chartCanvas) {
    createdChart = new DeviceChart(chartCanvas);
    return createdChart;
}

class DeviceChart {
    constructor(chartCanvas) {
        this.chart = new Chart(chartCanvas, {
            type: 'line',
            data: {
                labels: [],
                datasets: [{
                    label: "Value",
                    data: [],
                    backgroundColor: this.getRandomColor()
                }]
            },
            options: {
                elements: {
                    line: {
                        tension: 0 // disables bezier curves
                    }
                },
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        stacked: true,
                        gridLines: {
                            display: true,
                            color: "rgba(255,99,132,0.2)"
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false
                        }
                    }]
                }
            }
        });

    }

    unshiftValue(value) {
        this.chart.data.labels.push(this.chart.data.labels.length * 10);
        this.chart.data.datasets[0].data.unshift(value);
        this.chart.update();
    }

    clearChart(){
        this.chart.data.labels = [];
        this.chart.data.datasets[0].data = [];
        this.chart.update();
    }

    pushValue(value) {
        this.chart.data.labels.push(this.chart.data.labels.length * 10);
        this.chart.data.datasets[0].data.push(value);
        //remove first element
        this.chart.data.datasets[0].data.shift();
        this.chart.update();
    }

    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
}