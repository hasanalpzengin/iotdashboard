var deviceTypeRows;

function removeDeviceType(typeID){
    $.post('/sys-control/device-types',{
        removeDeviceType: typeID
    }).done(function(){
        window.location.reload();
    });
}

$('#search').on('input', function(){
    searchString = $('#search').val();
    
    deviceTypeRows.forEach(function(deviceTypeRow){
        if(!$(deviceTypeRow).find('name').text().includes(searchString)){
            deviceTypeRow.style.display = "none";
        }else{
            deviceTypeRow.style.display = "table-row";
        }
    });
});

$(document).ready(function(){
    deviceTypeRows = document.body.querySelectorAll(".device-type");
});