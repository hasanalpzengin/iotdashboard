function editUser(data){
    $.post('/sys-control/users/'+data.id,{
        id: data.id,
        username: data.username,
        password: data.password,
        name: data.name,
        surname: data.surname,
        mail: data.mail,
        user_type: data.user_type,
        birth_date: data.birth_date,
        group: data.group
    }).done(function(){
        window.location.href = "/sys-control/users";
    });
}

$('#form').submit(function(event){
    var data = {}
    data.id = $('#id').val();
    data.username = $('#username').val();
    data.password = $('#password').val();
    data.name = $('#name').val();
    data.surname = $('#surname').val();
    data.mail = $('#mail').val();
    data.birth_date = $('#birth_date').val();
    data.user_type = $('#user_type').val();
    data.group = $('#group').val();
    editUser(data);
    event.preventDefault();
});