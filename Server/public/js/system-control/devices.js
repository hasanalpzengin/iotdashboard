var deviceRows;
var datas = {};

function addDevice(datas){
    $.post('/sys-control/devices',{
        serial_number: datas.serial_number,
        name: datas.name,
        type: datas.type
    }).done(function(){
        window.location.reload();
    });
}

function removeDevice(deviceID){
    $.post('/sys-control/devices',{
        removeDevice: deviceID
    }).done(function(){
        window.location.reload();
    });
}

$('#addForm').submit(function(event){
    datas.serial_number = $('#device-serial-number').val();
    datas.name = $('#device-name').val();
    datas.type = $('#device-type').val();
    addDevice(datas);
    event.preventDefault();
});

$('#search').on('input', function(){
    searchString = $('#search').val();
    
    deviceRows.forEach(function(deviceRow){
        if(!$(deviceRow).find('name').text().includes(searchString)){
            deviceRow.style.display = "none";
        }else{
            deviceRow.style.display = "table-row";
        }
    });
});

$(document).ready(function(){
    deviceRows = document.body.querySelectorAll(".device");
});