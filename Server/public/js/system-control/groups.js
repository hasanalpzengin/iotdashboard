var groupRows;

function addGroup(groupName){
    $.post('/sys-control/groups',{
        addGroup: groupName
    }).done(function(){
        window.location.reload();
    });
}

function removeGroup(groupID){
    $.post('/sys-control/groups',{
        removeGroup: groupID
    }).done(function(){
        window.location.reload();
    });
}

$('#addForm').submit(function(event){
    groupName = $('#group-name').val();
    addGroup(groupName);
    event.preventDefault();
});

$('#search').on('input', function(){
    searchString = $('#search').val();
    
    groupRows.forEach(function(groupRow){
        if(!$(groupRow).find('name').text().includes(searchString)){
            groupRow.style.display = "none";
        }else{
            groupRow.style.display = "table-row";
        }
    });
});

$(document).ready(function(){
    groupRows = document.body.querySelectorAll(".group");
});