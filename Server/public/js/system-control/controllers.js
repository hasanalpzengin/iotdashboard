var controllerRows;

function addController(controllerName){
    $.post('/sys-control/controllers',{
        addController: controllerName
    }).done(function(){
        window.location.reload();
    });
}

function removeController(controllerID){
    $.post('/sys-control/controllers',{
        removeController: controllerID
    }).done(function(){
        window.location.reload();
    });
}

$('#addForm').submit(function(event){
    controllerName = $('#controller-name').val();
    addController(controllerName);
    event.preventDefault();
});

$('#search').on('input', function(){
    searchString = $('#search').val();
    
    controllerRows.forEach(function(controllerRow){
        if(!$(controllerRow).find('name').text().includes(searchString)){
            controllerRow.style.display = "none";
        }else{
            controllerRow.style.display = "table-row";
        }
    });
});

$(document).ready(function(){
    controllerRows = document.body.querySelectorAll(".controller");
});