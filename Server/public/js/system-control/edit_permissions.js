function editPermissions(userID, permissions){
    console.log(permissions);
    
    $.post(window.location,{
        userID: userID,
        permissions: permissions
    }).done(function(){
        //window.location.reload();
    });
}

$('#form').submit(function(event){
    var userID = $('#uid').val();
    var permissionInputs = document.body.querySelectorAll(".permission");
    var permissions = {}
    permissionInputs.forEach((input)=>{
        var value = $(input).is(":checked") ? "on": "off";
        permissions[$(input).attr('name')] = value;
    });

    editPermissions(userID, permissions);
    event.preventDefault();
});