var groupRows;

function addDeviceGroup(groupID, deviceSerialNumber){
    $.post('/sys-control/device-groups',{
        addGroup: groupID,
        device_serial: deviceSerialNumber
    }).done(function(){
        window.location.reload();
    });
}

function removeDeviceGroup(groupID, device_serial){
    $.post('/sys-control/device-groups',{
        removeGroup: groupID,
        device_serial: device_serial
    }).done(function(){
        window.location.reload();
    });
}

$('#addForm').submit(function(event){
    groupID = $('#group').val();
    deviceSerialNumber = $('#serial').val();
    addDeviceGroup(groupID, deviceSerialNumber);
    event.preventDefault();
});

$('#search').on('input', function(){
    searchString = $('#search').val();
    
    groupRows.forEach(function(groupRow){
        if(!$(groupRow).find('name').text().includes(searchString)){
            groupRow.style.display = "none";
        }else{
            groupRow.style.display = "table-row";
        }
    });
});

$(document).ready(function(){
    groupRows = document.body.querySelectorAll(".group");
});