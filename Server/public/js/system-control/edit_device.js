var data = {}

function editDevice(data){
    $.post('/sys-control/devices/'+data.serial_number,{
        serial_number: data.serial_number,
        name: data.name,
        type: data.type
    }).done(function(){
        window.location.href = "/sys-control/devices";
    });
}

$('#form').submit(function(event){
    data.serial_number = $('#serial_number').val();
    data.name = $('#name').val();
    data.type = $('#type').val();
    editDevice(data);
    event.preventDefault();
});