var groupRows;

function addUser(datas){
    console.log("Hi");
    $.post('/sys-control/users',{
        uname: datas.username,
        upass: datas.password,
        name: datas.name,
        surname: datas.surname,
        mail: datas.mail,
        birth_date: datas.birth_date,
        group: datas.group
    }).done(function(){
        window.location.reload();
    });
}

function removeUser(userID){
    $.post('/sys-control/users',{
        removeUser: userID
    }).done(function(){
        window.location.reload();
    });
}

$('#addForm').submit(function(event){
    var datas = {};
    datas.username = $('#username').val();
    datas.password = $('#password').val();
    datas.name = $('#name').val();
    datas.surname = $('#surname').val();
    datas.mail = $('#mail').val();
    datas.date = $('#date').val();
    datas.group = $('#group').val();
    addUser(datas);
    event.preventDefault();
});

$('#search').on('input', function(){
    searchString = $('#search').val();
    
    userRows.forEach(function(userRow){
        if($(userRow).find('id').text().includes(searchString) || $(userRow).find('username').text().includes(searchString)){
            userRow.style.display = "table-row";
        }else{
            userRow.style.display = "none";
        }
    });
});

$(document).ready(function(){
    userRows = document.body.querySelectorAll(".user");
});