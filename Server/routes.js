module.exports = function (app) {
    //route
    app.get('/', function (req, res) {
        if (!isLogged(req)) {
            console.log("Login Requested");
            res.render('pages/login');
            return;
        }
        //login controller
        var file = require("./controllers/LoginController");
        login = new file();
        login.getIndex(req, res);
    });
    //login page
    app.get('/login', function (req, res) {
        res.render('pages/login');
    });
    //settings
    app.get('/settings', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        } else {
            var file = require('./controllers/LoginController');
            var login = new file();
            login.getSettings(req, res);
        }
    });

    //device list
    app.get('/devices', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }
        var file = require("./controllers/DeviceController");
        var devices = new file();
        devices.listDevices(req, res);
    });

    //device list
    app.get('/device/settings/:serial_number', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }
        var file = require("./controllers/DeviceController");
        var devices = new file();
        devices.deviceSettings(req, res);
    });

    //device list
    app.post('/device/settings', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }
        var file = require("./controllers/DeviceController");
        var devices = new file();
        devices.editSettings(req, res);
    });

    //device list
    app.get('/history', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }
        var file = require("./controllers/LogController");
        var log = new file();
        log.showHistory(req, res);
    });
    //device dashboard
    app.get('/devices/:serial_number', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }

        var file = require("./controllers/DeviceController");
        var devices = new file();
        devices.viewDevice(req, res);

    });

    app.get('/sys-control/devices/:serial_number', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }
        var file = require("./controllers/admin/DeviceController");
        var devices = new file();
        devices.viewDevice(req, res);
    });

    app.post('/sys-control/devices/:serial_number', function (req, res) {
        var file = require("./controllers/admin/DeviceController");
        var devices = new file();
        devices.editDevice(req, res);
    });

    app.get('/sys-control/device-groups/:serial_number', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }

        var file = require("./controllers/admin/DeviceGroupsController");
        var device_groups = new file();
        device_groups.viewDeviceGroups(req, res);
    });

    app.get('/sys-control/permissions/:id', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }

        var file = require("./controllers/admin/PermissionController");
        var permissions = new file();
        permissions.showEditPermissions(req, res);
    });

    app.post('/sys-control/permissions/:id', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }

        var file = require("./controllers/admin/PermissionController");
        var permissions = new file();
        permissions.editPermissions(req, res);
    });

    app.get('/sys-control/devices', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }
        //controller
        var file = require("./controllers/admin/DeviceController");
        var devices = new file();
        devices.listDevices(req, res);
    });

    app.post('/sys-control/devices', function (req, res) {
        if (typeof req.session.uid != 'undefined') {
            var file = require("./controllers/admin/DeviceController");
            var device = new file();
            if (typeof req.body.serial_number != 'undefined') {
                device.addDevice(req, res);
            }
            if (typeof req.body.removeDevice != 'undefined') {
                device.removeDevice(req, res);
            }

        }
    });

    app.get('/sys-control/groups', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }
        var file = require("./controllers/admin/GroupController");
        var group = new file();
        group.listGroups(req, res);
    });

    app.post('/sys-control/groups', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }

        var file = require("./controllers/admin/GroupController");
        var group = new file();
        if (typeof req.body.addGroup != 'undefined') {
            group.addGroup(req, res);
        }
        if (typeof req.body.removeGroup != 'undefined') {
            group.removeGroup(req, res);
        }
    });

    app.post('/sys-control/device-groups', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }
        var file = require("./controllers/admin/DeviceGroupsController");
        var device_groups = new file();
        if (typeof req.body.addGroup != 'undefined') {
            device_groups.addDeviceGroup(req, res);
        }
        if (typeof req.body.removeGroup != 'undefined') {
            device_groups.removeDeviceGroup(req, res);
        }
    });

    app.get('/sys-control/controllers', function (req, res) {
        if (!isLogged(req)) {
            res.render('pages/login');
            return;
        }
        var file = require("./controllers/admin/ControllerController");
        var controller = new file();
        controller.listControllers(req, res);
    });

    app.post('/sys-control/controllers', function (req, res) {
        if (typeof req.session.uid != 'undefined') {
            var file = require("./controllers/admin/ControllerController");
            var controller = new file();
            if (typeof req.body.addController != 'undefined') {
                controller.addController(req, res);
            }
            if (typeof req.body.removeController != 'undefined') {
                controller.removeController(req, res);
            }
        }
    });

    app.get('/sys-control/device-types', function (req, res) {
        if (typeof req.session.uid != 'undefined') {
            var file = require("./controllers/admin/DeviceTypesController");
            var types = new file();
            types.listDeviceTypes(req, res);
        } else {
            res.render('pages/login');
            return;
        }
    });

    app.post('/sys-control/device-types', function (req, res) {
        if (typeof req.session.uid != 'undefined') {
            var file = require("./controllers/admin/DeviceTypesController");
            var types = new file();
            if (typeof req.body.name != 'undefined') {
                types.addDeviceType(req, res);
            }
            if (typeof req.body.removeDeviceType != 'undefined') {
                console.log("Worked");
                types.removeDeviceType(req, res);
            }
        }
    });

    app.get('/sys-control/users', function (req, res) {
        if (typeof req.session.uid != 'undefined') {
            var file = require("./controllers/admin/UserController");
            var users = new file();
            users.listUsers(req, res);
        } else {
            res.render('pages/login');
            return;
        }
    });

    app.get('/sys-control/users/:id', function (req, res) {
        if (typeof req.session.uid != 'undefined') {
            var file = require("./controllers/admin/UserController");
            var users = new file();
            users.getEditUser(req, res);
        } else {
            res.render('pages/login');
            return;
        }
    });

    app.post('/sys-control/users/:id', function (req, res) {
        if (typeof req.session.uid != 'undefined') {
            var file = require("./controllers/admin/UserController");
            var users = new file();
            users.editUser(req, res);
        } else {
            res.render('pages/login');
            return;
        }
    });

    app.post('/sys-control/users', function (req, res) {
        if (typeof req.session.uid != 'undefined') {
            var file = require("./controllers/admin/UserController");
            var users = new file();
            if (typeof req.body.uname != 'undefined') {
                users.addUser(req, res);
            }
            if (typeof req.body.removeUser != 'undefined') {
                users.removeUser(req, res);
            }
        }
    });

    app.get('/sys-control', function (req, res) {
        if (typeof req.session.uid != 'undefined') {
            res.render('pages/system-control');
            return;
        } else {
            res.render('pages/login');
            return;
        }
    });

    //index post = LOGIN
    app.post('/', function (req, res) {
        var file = require('./controllers/LoginController');
        var login = new file();
        login.makeLogin(req, res, (err, data) => {
            if (err) throw err;
            console.log("Resulted");

            req.session.uid = data.uid;
            req.session.utype = data.utype;
            req.session.group = data.group;
            res.redirect("/");
        });
    });

    //index post = LOGIN
    app.get('/logout', function (req, res) {
        var file = require('./controllers/LoginController');
        var login = new file();
        login.disconnect(req, res);
    });

    app.post('/devices', function (req, res) {
        if (isLogged(req)) {
            var file = require("./controllers/DeviceController");
            var deviceController = new file();
            //request defined check
            if (typeof req.body.removeSerial == 'undefined' && typeof req.body.addSerial == 'undefined') {
                res.render('pages/error', {
                    error: 'Request is not defined'
                });
                return;
            } else if (typeof req.body.removeSerial != 'undefined') {
                deviceController.removeDevice(req, res);
            } else {
                deviceController.addDevice(req, res);
            }
        }
    });

    /* API */
    app.get('/api/devices', (req, res) => {
        if (isLogged(req)) {
            var file = require("./controllers/ApiController");
            var apiController = new file();
            apiController.listDevices(req, res);
        }
    });

    app.get('/api/device/:serial_number', (req, res) => {
        if (isLogged(req)) {
            var file = require("./controllers/ApiController");
            var apiController = new file();
            apiController.viewDevice(req, res);
        }
    });

    app.post('/api/login', (req, res) => {
        var file = require("./controllers/ApiController");
        var apiController = new file();
        apiController.makeLogin(req, res);
    });

    app.get('/api/login', (req, res) => {
        var file = require("./controllers/ApiController");
        var apiController = new file();
        apiController.loginCheck(req, res);
    });

    app.post('/api/filterValues', (req, res)=>{
        var file = require("./controllers/ApiController");
        var apiController = new file();
        apiController.filterValues(req, res);
    });
}

function isLogged(req) {
    return ((typeof req.session.uid == 'undefined') ? false : true);
}