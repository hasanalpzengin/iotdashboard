const mysql = require('mysql');

getPool = function(){
    var pool = mysql.createPool({
        host: "localhost",
        user: "root",
        password: "",
        database: "iotDashboard",
        queueLimit: 3,
        connectionLimit: 2
    });
    return pool;
}

module.exports = getPool;