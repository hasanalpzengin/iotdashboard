const express = require('express');
const socket = require('socket.io');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const bodyFileUpload = require('express-fileupload');
const socketCookieParser = require('socket.io-cookie-parser');
const session = require('express-session');

//import ipLocator to create map in index page
const ipLocator = require('ip-locator');

const app = express();
const server = app.listen(3000);
const http = require('http');
//socket variables
var coordinate_list = [];
var client_count = 0;
var ip_list = [];
var mysql = require('./db/mysql');

//set public folder as root front-end directory
app.use(express.static(__dirname + '/public'));
//express body parser for requests
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyFileUpload());
//init cookie and session
app.use(cookieParser());
app.use(session({
    secret: "IoT Management Panel",
    proxy: true,
    resave: true,
    saveUninitialized: true
}));

app.use(function (req, res, next) {
    res.locals.utype = req.session.utype;
    next();
});
//set views folder for ejs view engine
var path = require('path');
app.set('views', path.join('public', 'views'));
app.set('view engine', 'ejs');
//init routes
require("./routes")(app);

var io = socket(server);
io.use(socketCookieParser());
console.log("My socket server is running");

io.sockets.on('connection', (socket)=>{
    io.sockets.emit('updateClientCount', ++client_count);
    console.log("New Connection: " + socket.id);
    //get ip address to create map
    var address = socket.handshake.address;
    //if not exist in array
    if (ip_list.indexOf(address.address) <= -1 && address.address != '::1') {
        ip_list.push(address.address);
        /*
        ipLocator.getDomainOrIPDetails(address.address, 'json', function (err, data) {
            coordinate_list.push([data.lat, data.lon]);
        });
        */
    }

    socket.on('setSerialNumber', function (msg) {
        mysql().getConnection((err, sqlConn)=>{
            var data = [msg, socket.serial_number];
            var sql = "UPDATE devices SET serial_number=? WHERE serial_number=?";
            sqlConn.query(sql, data, function (err, result) {
                if (err) throw err;
                console.log("Device's serial changed");
            });
            socket.serial_number = msg;
            console.log("Socket serial changed to: " + socket.serial_number);
        });
    });

    socket.on('value', function (msg) {
        if (typeof socket.serial_number != 'undefined') {
            //create log
            mysql().getConnection((err, sqlConn)=>{
                var datetime = new Date().toISOString().slice(0, 19).replace('T', ' ');
                var data = [socket.serial_number, datetime, msg];
                var sql = "INSERT INTO device_logs(dserial, created_at, event, message) VALUES(?, ?,'value',?)";
                sqlConn.query(sql, data, function (err, result) {
                    if (err) throw err;
                });
                sendValue(socket, msg);
            });
        }
    });

    //create device if it isn't exist
    socket.on('deviceSettings', function (msg) {
        deviceSettings(msg, function (err, results) {
            if (err) throw err;
            socket.serial_number = results.serial_number;
            socket.name = results.name;
            socket.type = results.type;
        });
    });

    socket.on('join', function(msg){
        console.log("Joined to: "+msg);
        socket.join(msg);
    });

    socket.on('disconnect', ()=>{
        mysql().getConnection((err, sqlConn)=>{
            if (typeof socket.serial_number != 'undefined') {
                var data = [socket.serial_number];
                var sql = "UPDATE devices SET is_online=0 WHERE serial_number=?";
                sqlConn.query(sql, data, function (err, result) {
                    if (err) throw err;
                });
            }
        });
        console.log(socket.id + " has disconnected");
        io.sockets.emit('updateClientCount', --client_count);
    });
});

io.sockets.on('sendValue', (data)=>{
    io.sockets.in(socket.serial_number).emit('value', value);
});

function sendValue(socket, value) {
    io.sockets.in(socket.serial_number).emit('value', socket.serial_number+"@"+value);
}

//get or set device
function deviceSettings(msg, callback) {
    var settings = msg.split("@");
    console.log(settings);
    
    mysql().getConnection((err, sqlConn)=>{
        if (settings[0] != "") {
            //is exist in db
            var data = [settings[0]];
            var sql = "SELECT * FROM devices WHERE serial_number=?";
            sqlConn.query(sql, data, function (err, result) {
                if (err) throw err;
                if (result.length <= 0) {
                    //not exist in db
                    mysql().getConnection((err, sqlConn)=>{
                        var data = [settings[0], datetime];
                        var datetime = new Date().toISOString().slice(0, 19).replace('T', ' ');
                        var sql = "INSERT INTO devices(serial_number, last_connection) VALUES(?,?)";
                        sqlConn.query(sql, data, function (err, result) {
                            if (err) throw err;
                            console.log("Device Registered");
                        });
                    });
                } else {
                    //not exist in db
                    mysql().getConnection((err, sqlConn)=>{
                        var data = [datetime ,settings[0]];
                        var datetime = new Date().toISOString().slice(0, 19).replace('T', ' ');
                        var sql = "UPDATE devices SET last_connection=?, is_online=1 WHERE serial_number=?";
                        console.log("online status updated");
                        
                        sqlConn.query(sql, data, function (err, result) {
                            if (err) throw err;
                        });
                    });
                }
                mysql().getConnection((err, sqlConn)=>{
                    var data = [settings[0]];
                    var sql = "SELECT * FROM devices WHERE serial_number=?";
                    sqlConn.query(sql, data, function (err, result) {
                        if (err) throw err;
                        console.log("Device Logged");
                        callback(null, result[0]);
                    });
                });
            });
        }
    });
}
