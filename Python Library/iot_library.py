# debugging
import logging
logging.getLogger('socketIO-client').setLevel(logging.DEBUG)
logging.basicConfig()
#socket
import requests
from socketIO_client_nexus import SocketIO, BaseNamespace
from base64 import b64encode
#settings file check
from pathlib import Path
#threading
from threading import Thread

class Namespace(BaseNamespace):
    def on_connect(self):
        print('Connected')
    def on_reconnect(self):
        print('Reconnected')
    def on_disconnect(self):
        print('Disconnected')

class Connection(object):
    settings = {}
    settings_file = Path("settings.conf")

    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.socketIO = SocketIO(self.host, self.port, Namespace)
        self.socketIO.on('connect', self.on_connect)
        self.listenerThread = Thread(target=self._listenerThread)
        self.listenerThread.start()

    def on_connect(self):
        print("Connected. run setDevice to complete setup")

    def sendCommand(self, event, data):
        self.socketIO.emit(event, data)

    def sendValue(self, value):
        if(len(str(self.settings['serial_number']))>0):
            self.socketIO.emit('value', value)
        else:
            print("You don't have a serial number to communicate")
        

    def setDevice(self, serialNumber):
        if(self.settings_file.is_file()):
            selection = input("Device configs already exist. Do you still want to set new device(y/N): \n")
            if(selection == 'y' or selection == 'Y'):
                self.settings['serial_number'] = serialNumber
                self.saveSettings()
            else:
                print("Settings can be loaded with readSettings() method")
                self.readSettings()
        else:
            self.settings['serial_number'] = serialNumber
            self.saveSettings()
    
    def readSettings(self):
        if self.settings_file.is_file():
            with open('settings.conf') as f:
                settingsData = f.read()
                #settings = settingsData.split("@")
                self.settings['serial_number'] = settingsData
                self.sendCommand("deviceSettings",settingsData)
                print("Device Settings Sent")
        else:
            print("File is not exist, run saveSettings method before reading it.")
    # save all settings to settings attribute and return plain string
    def saveSettings(self):
        try:
            settingsData = ""
            with open('settings.conf', 'w') as f:
                f.write(self.settings['serial_number'])
                settingsData = self.settings['serial_number']
                self.sendCommand("deviceSettings",settingsData)
            return settingsData
        except NameError:
            print("Device is not defined, run setDevice method before saving it.")

    def _listenerThread(self):
        while(True):
            self.socketIO.wait(seconds=1)